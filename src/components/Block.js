import React from 'react'
import logo from '../logo.svg';

class Block extends React.Component {

    getValue(){
        if (!this.props.value.isShown){
            return this.props.value.isFlagged ? "🚩" : null;
        }
        if (this.props.value.isMine) {
            return ( <img className="react" alt="React Icon" src={ logo } /> )
        }
        if(this.props.value.neighbour === 0 ){
            return null;
        }
        return this.props.value.neighbour;
    }

    render(){
        let className = "block" + (this.props.value.isShown ? "" : " hidden") + (this.props.value.isMine ? " is-mine" : "") + (this.props.value.isFlagged ? " is-flag" : "");

        return (
            <div ref="block" onClick={this.props.onClick} className={className} onContextMenu={this.props.cMenu}>
                {this.getValue()}
            </div>
        )
    }
}

export default Block