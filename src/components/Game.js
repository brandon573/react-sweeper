import React from 'react'
import Board from './Board'
import Dashboard from './Dashboard'

class Game extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
            height: 8,
            width: 8,
            mines: 10,
            status: false
        };
        this.updateMines = this.updateMines.bind(this);
        this.updateStatus = this.updateStatus.bind(this);
    }

    updateMines (mines) {
        this.setState({
            mines: mines
        })
    }

    updateStatus (action) {
        this.setState({
            status: action
        })
    }

    render() {
        const { height, width, mines, status } = this.state;
        return (
        <div className="game">
            <Dashboard mines={mines} updateMines={this.updateMines} status={status} />
            <Board height={height} width={width} mines={mines} updateMines={this.updateMines} status={status} updateStatus={this.updateStatus} />
        </div>
        )
    }
}

export default Game