import React from 'react'

class MineCounter extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        clockTick: 0,
        clockTime: 0,
        mines: this.props.mines
      };
    }

    render () {
        return (
            <div className="counter"><span className="title">Mines</span> <span className="count">{ this.state.mines }</span></div>
        )
    }
}

export default MineCounter