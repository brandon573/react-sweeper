import React from 'react';

class Clock extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clockTick: 0,
      clockTime: 0,
      status: this.props.status,
    };
  }

  startClock() {
    var clockTick = setInterval(this.clockSecond.bind(this), 1000);
    this.setState({ clockTick: clockTick });
  }

  pauseClock() {
    clearInterval(this.state.clockTick);
    this.setState({ clockTick: null });
  }

  resetClock() {
    clearInterval(this.state.clockTick);
    this.setState({ clockTime: 0, clockTick: null });
  }

  clockSecond() {
    let clocked = this.state.clockTime + 1;
    this.setState({ clockTime: clocked });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.status === nextProps.status)
      return;

    switch (nextProps.status) {
      case 'new':
        this.resetClock();
        break;
      case 'playing':
        this.startClock();
        break;
      case 'winner':
      case 'loser':
      default:
        this.pauseClock();
        break;
    }
  }

  render() {

    return (
      <div className="clock"><span className="title">Time</span> <span className="time">{this.state.clockTime} </span></div>
    );
  }
}

export default Clock;