import React from 'react';
import { mount, shallow } from 'enzyme';
import Clock from './Clock';
import Game from './Game';

describe('Test Clock', () => {

    it('clock renders based on snapshot', () => {
        const component = shallow(<Clock />);
        expect(component).toMatchSnapshot(); 
    });

    it('clock starts when first block is clicked', () => {
        const component = mount(<Game />);
        component
          .find('div.block.hidden').first()
          .simulate('click');

        setTimeout(() => {
            let clockTime = component.find(".time");
            expect(clockTime.text()).toBe("3");
            component.unmount();
        }, 3000);
    });

    it('clock\'s start, pause, reset methods working', () => {
        const component = shallow(<Clock />);
        const instance = component.instance();

        instance.startClock();
        setTimeout(() => {
            expect(component.state('clockTime')).toBe(3);
        }, 3000);
        
        instance.pauseClock();
        setTimeout(() => {
            expect(component.state('clockTime')).toBe(3);
        }, 3000);

        instance.resetClock();
        expect(component.state('clockTime')).toBe(0);

    });
});