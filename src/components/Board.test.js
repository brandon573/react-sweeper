import React from 'react';
import { shallow } from 'enzyme';
import Board from './Board';

describe('Test board setup', () => {

    it('renders correctly based on snapshot', () => {
        const board = shallow(<Board />);
        expect(board).toMatchSnapshot();
    });

});