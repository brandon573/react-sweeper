import React from 'react'
import Clock from './Clock'
//import MineCounter from './MineCounter';

class Dashboard extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        clockTick: 0,
        clockTime: 0,
        status: this.props.status,
        mines: this.props.mines
      };
    }

    render () {
        return (
            <div id="dashboard">
                <h1 className="title">React Sweeper</h1>
                <Clock status={this.props.status} />
                <div className="counter"><span className="title">Mines</span> <span className="count">{ this.props.mines }</span></div>
            </div>
        )
    }
}

export default Dashboard