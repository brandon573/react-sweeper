import React from 'react'
import Block from './Block'

class Board extends React.Component {

    constructor (props) {
        super(props)
        this.state = {
            boardData: this.createBoardData(this.props.height, this.props.width, this.props.mines),
            status: this.props.status,
        };

        this.handleClick = this.handleClick.bind(this);
        this.handleRightClick = this.handleRightClick.bind(this);
    }


    createBoardData(height, width, mines) {
        let data = this.createEmptyArray(height, width);
        data = this.placeMines(data, height, width, mines);
        data = this.getBorderBlocks(data, height, width);
        
        return data;
    }

    createEmptyArray(height, width) {
        let data = [];
        for (let i = 0; i < height; i++) {
          data.push([]);
          for (let j = 0; j < width; j++) {
            data[i][j] = {
              x: i,
              y: j,
              isMine: false,
              neighbour: 0,
              isShown: false,
              isEmpty: false,
              isFlagged: false,
              };
            }
          }
          return data;
    }

    placeMines(data, height, width, mines) {
        let randomx, randomy, minesPlanted = 0;
        while (minesPlanted < mines) {
            randomx = this.getRandomNumber(width);
            randomy = this.getRandomNumber(height);
            if (!(data[randomx][randomy].isMine)) {
            data[randomx][randomy].isMine = true;
            minesPlanted++;
            }
        }
        return (data);
    }

    getBorderBlocks(data, height, width) {
        let updatedData = data;
        for (let i = 0; i < height; i++) {
            for (let j = 0; j < width; j++) {
            if (data[i][j].isMine !== true) {
                let mine = 0;
                const area = this.traverseBoard(data[i][j].x, data[i][j].y, data);
                area.map(value => {
                    if (value.isMine) {
                        return mine++;
                    } else {
                        return false;
                    }
                })
                if (mine === 0) {
                updatedData[i][j].isEmpty = true;
                }
                updatedData[i][j].neighbour = mine;
            }
            }
        }
        return (updatedData);
    }


    traverseBoard(x, y, data) {
        const el = [];
        //up
        if (x > 0) {
            el.push(data[x - 1][y]);
        } 
        //down
        if (x < this.props.height - 1) {
            el.push(data[x + 1][y]);
        }
        //left
        if (y > 0) {
            el.push(data[x][y - 1]);
        }
        //right
        if (y < this.props.width - 1) {
            el.push(data[x][y + 1]);
        }
        // top left
        if (x > 0 && y > 0) {
            el.push(data[x - 1][y - 1]);
        }
        // top right
        if (x > 0 && y < this.props.width - 1) {
            el.push(data[x - 1][y + 1]);
        }
        // bottom right
        if (x < this.props.height - 1 && y < this.props.width - 1) {
            el.push(data[x + 1][y + 1]);
        }
        // bottom left
        if (x < this.props.height - 1 && y > 0) {
            el.push(data[x + 1][y - 1]);
        }
        return el;
    }

    getMines(data) {
        let mineArray = [];

        data.map(datarow => {
            return datarow.map((dataitem) => {
                if (dataitem.isMine) {
                    return mineArray.push(dataitem);
                } else {
                    return false;
                }
            });
        });

        return mineArray;
    }

    getFlags(data) {
        let mineArray = [];

        data.map(datarow => {
            return datarow.map((dataitem) => {
                if (dataitem.isFlagged) {
                    return mineArray.push(dataitem);
                } else {
                    return false;
                }
            });
        });

        return mineArray;
    }

    getHidden(data) {
        let mineArray = [];

        data.map(datarow => {
            return datarow.map((dataitem) => {
                if (!dataitem.isShown) {
                    return mineArray.push(dataitem);
                } else {
                    return false;
                }
            });
        });

        return mineArray;
    }

    getRandomNumber(dimension) {
        return Math.floor((Math.random() * 1000) + 1) % dimension;
    }

    revealBoard() {
        let updatedData = this.state.boardData;
        updatedData.map((datarow) => {
            return datarow.map((dataitem) => {
                dataitem.isShown = true;
                return dataitem.isShown;
            });
        });
        this.setState({
            boardData: updatedData
        })
    }

    revealEmpty(x, y, data) {
        let area = this.traverseBoard(x, y, data);
        area.map(value => {
            if (!value.isShown && (value.isEmpty || !value.isMine)) {
                //if (value.isFlagged) {
                    //remove any placed flags
                    //data[value.x][value.y].isFlagged = false;
                    //let mines = this.props.mines;
                    //this.props.updateMines(mines--);
                //}
                data[value.x][value.y].isShown = true;
                if (value.isEmpty) {
                    this.revealEmpty(value.x, value.y, data);
                }
            }
            return true;
        });
        return data;
    }

    handleClick(x, y) {
        let status = "playing";

        //if revealed, return true.
        if (this.state.boardData[x][y].isShown) return null;

        //if mine, game over
        if (this.state.boardData[x][y].isMine) {
            status = "loss";
            this.handleLoss();
        }

        let updatedData = this.state.boardData;
        updatedData[x][y].isFlagged = false;
        updatedData[x][y].isShown = true;

        if (updatedData[x][y].isEmpty) {
            updatedData = this.revealEmpty(x, y, updatedData);
        }

        if (this.getHidden(updatedData).length === this.props.mines) {
            status = "win";
            this.handleWin();
        }

        this.props.updateStatus(status);

        this.setState({
            boardData: updatedData,
            status: status,
        });
    }

    handleRightClick(e, x, y) {
        e.preventDefault();
        let updatedData = this.state.boardData;
        let mines = this.props.mines;
        let status = false;

        // check if already revealed
        if (updatedData[x][y].isShown) return;

        if (updatedData[x][y].isFlagged) {
            updatedData[x][y].isFlagged = false;
            this.props.updateMines(mines++);
        } else {
            updatedData[x][y].isFlagged = true;
            mines--;
        }

        if (mines === 0) {
            const mineArray = this.getMines(updatedData);
            const FlagArray = this.getFlags(updatedData);
            let win = (JSON.stringify(mineArray) === JSON.stringify(FlagArray));
            if (win) {
                status = "win";
                this.handleWin();
            }
        }

        this.props.updateMines(mines);
        this.props.updateStatus(status);

        this.setState({
            boardData: updatedData,
            status: status,
        });
    }

    handleWin() {
        this.revealBoard();
    }

    handleLoss() {
        this.revealBoard();
    }

    renderBoard(data) {
        return data.map((datarow) => {
            return datarow.map((dataitem) => {
            return (
                <div 
                key={dataitem.x * datarow.length + dataitem.y}>
                <Block
                    onClick={() => this.handleClick(dataitem.x, dataitem.y)}
                    cMenu={(e) => this.handleRightClick(e, dataitem.x, dataitem.y)}
                    value={dataitem}
                />
                {(datarow[datarow.length - 1] === dataitem) ? <div className="clear" /> : ""}
                </div>
            );
            })
        });
    }

    render() {

        return (
            <div className={"board " + this.state.status }>
                { this.renderBoard(this.state.boardData) }
            </div>
            );
        }
    }

export default Board